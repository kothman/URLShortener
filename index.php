<?php
    // If query parameter 'q' is not empty, then this is a redirect request
    if (!empty($_GET['q'])) {
        require 'redirect.php';
        exit();
    }
?>
<!DOCTYPE html>

<html>
    <head>
        <title>Url Shortener Demo</title>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="main.css">
        <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
        <script src="main.js"></script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="page-header">URL Shortener <span class="h3 pull-right"><a href="list.php">List All URLs</a></span></h1>
                </div>
            </div>
            <div class="row padding-top">
                <div class="col-xs-12">
                    <form class="form" id="url-form">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label for="original-url"><h3>Your URL</h3></label>
                                    <input type="url" class="form-control" name="original-url" placeholder="https://domain.com/something" id="original-url">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label for="shortened-url"><h3>Shortened URL <span class="text-muted">(Optional)</span></h3></label>
                                    <div class="input-group">
                                        <div class="input-group-addon">http://<?= $_SERVER['SERVER_NAME']; ?>/</div>
                                        <input type="text" class="form-control" name="shortened-url" id="shortened-url">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-12">
                                    <button type="button" id="button" class="btn btn-primary">
                                        <span id="shorten">Shorten</span>
                                        <span id="loading" class="saving"><span>&#9673</span><span>&#9673</span><span>&#9673</span></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row row-center padding-top">
                <div class="col-xs-12">
                    <div class="alert alert-info" id="success">This is the result</div>
                    <div class="alert alert-danger" id="error">This is the result</div>
                </div>
            </div>
        </div>
    </body>
</html>