<?php
    use Models\UrlModel;
    require 'autoload.php';
    
    $rows = (new UrlModel())->get();
?>
<!DOCTYPE html>

<html>
    <head>
        <title>Url Shortener Demo</title>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="main.css">
    </head>
    <body>
        <div class="container">
            <div class="row page-header">
                <div class="col-xs-12">
                    <h1 class="">Shortened URLs <span class="text-muted h3"><a href="./">(back)</a></span></h1>
                </div>
            </div>
            <div class="row padding-top">
                <div class="col-xs-12">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Full URL</th>
                                <th>Shortened URL</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($rows as $row): ?>
                                <tr>
                                    <td><?= $row['id']; ?></td>
                                    <td><a href="<?= $row['redirectUrl']; ?>"><?= $row['redirectUrl']; ?></a></td>
                                    <td><a href="<?= $row['shortenedPart']; ?>"><?= $row['shortenedPart']; ?></a></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>