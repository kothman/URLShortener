
var Page = {};

// deffer any action until the page has loaded
document.addEventListener("DOMContentLoaded", function() {Page.start();});
Page.start = function () {
    Page.fetchElements();
    Page.setDefaultStyling();
    Page.setEventListeners();
};

Page.fetchElements = function () {
    Page.shorten = document.getElementById("shorten");
    Page.loading = document.getElementById("loading");
    Page.success = document.getElementById("success");
    Page.error = document.getElementById("error");
    Page.button = document.getElementById("button");
    Page.originalUrl = document.getElementById("original-url");
    Page.shortenedUrl = document.getElementById("shortened-url");
};

Page.setDefaultStyling = function() {
    Page.hide(Page.loading);
    Page.hide(Page.success);
    Page.hide(Page.error);
};

Page.setEventListeners = function () {
    Page.button.addEventListener("click", Page.sendUrl);
};

Page.removeEventListeners = function () {
    Page.button.removeEventListener("click", Page.sendUrl);
};

Page.hide = function (elem) {
    elem.style.display = "none";
};

Page.show = function (elem) {
    elem.style.display = "block";
};

Page.sendUrl = function() {
    Page.removeEventListeners();
    Page.hide(Page.shorten);
    Page.show(Page.loading);
    
    console.log("Sending url " +
                Page.originalUrl.value +
                " to be shortened to " +
                Page.shortenedUrl.value);
    /*
    $.post("create.php",
           {originalUrl: Page.originalUrl.value,
           shortenedUrl: Page.shortenedUrl.value})
        .done( function(data) {Page.responseCallback(data);})
        .fail(function() { alert("There was an error sending the data."); });
    */
    var req = new XMLHttpRequest();
    var url = "create.php";
    var params = Page.buildPostParams();
    req.open("POST",url, true);
    req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    req.onreadystatechange = function() { //Call a function when the state changes.
        if(req.readyState == 4 && req.status == 200) {
            Page.responseCallback(req.responseText);
        } else {
            Page.resetStatusView();
            Page.show(Page.error);
            Page.error.innerHTML = "There was an error sending your request.";
        }
    };
    req.send(params);
};

Page.responseCallback = function(data) {
    Page.response = JSON.parse(data);
    console.log(Page.response);
    Page.hide(Page.loading);
    Page.show(Page.shorten);
    
    Page.resetStatusView();
    
    if(Page.response.status === "success") {
        Page.show(Page.success);
        Page.success.innerHTML = Page.response.message + "<br/>" + Page.buildLink();
    } else {
        Page.show(Page.error);
        Page.error.innerHTML = Page.response.message;
    }

    Page.setEventListeners();
};

Page.resetStatusView = function () {
    Page.hide(Page.success);
    Page.hide(Page.error);
}

Page.buildLink = function () {
    var url = window.location.href + Page.response.newUrl;
    var html = '<a href="' + url + '">' + url + '</a>';
    return html;
}

Page.buildPostParams = function() {
    return "originalUrl=" + Page.originalUrl.value + "&shortenedUrl=" + Page.shortenedUrl.value;
}