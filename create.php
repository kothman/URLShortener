<?php
use Controllers\UrlController;

require 'autoload.php';

$UrlController = new UrlController();
$response = $UrlController->create($_POST['originalUrl'], $_POST['shortenedUrl']);

echo $response;