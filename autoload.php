<?php
spl_autoload_register(function ($class_name) {
    $parts = explode('\\', $class_name);
    $filePath = implode(DIRECTORY_SEPARATOR,$parts) . '.php';
    if (file_exists($filePath)) {
        include $filePath;
    }
});

