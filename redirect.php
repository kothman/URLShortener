<?php
use Controllers\RedirectController;

require 'autoload.php';

$RedirectController = new RedirectController();
// remove the first character, since this will be a slash
$shortenedUrl = substr($_GET['q'], 1);

$RedirectController->redirect($shortenedUrl);