<?php
// MySQL database config
$DATABASE_CONFIG = [
    'database' => 'url_shortener',
    'host' => '127.0.0.1',
    'port' => '3306',
    'username' => 'root',
    'password' => ''
];