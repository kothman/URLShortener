<?php
namespace Models;

class Model {
    protected $DB_INFO;
    protected $DB_CONN;
    protected $DB_TABLE;
    
    public function __construct() {
        require 'database.php';
        $this->DB_TABLE = $this->tableName();
        $this->DB_INFO = $DATABASE_CONFIG;
        $this->DB_CONN = new \mysqli($this->DB_INFO['host'],
                                        $this->DB_INFO['username'],
                                        $this->DB_INFO['password'],
                                        $this->DB_INFO['database'],
                                        $this->DB_INFO['port']);
        
        if ($this->DB_CONN->connect_error) {
            throw new \Exception('Unable to connect to the database: ' . $this->DB_CONN->connect_error);
        }
    }
    
    public function __destruct() {
        $this->DB_CONN->close();
    }
    
    private function tableName() {
        $className = get_class($this);
        $startPos = strpos($className, '\\') + 1;
        $endPos = strrpos($className, 'Model');
        $numOfChars = $endPos - $startPos;
        
        return strtolower(substr($className, $startPos, $numOfChars)) . 's';
    }
}