<?php
namespace Models;

class UrlModel extends Model {

    public function __construct() {
        parent::__construct();
    }

    public function create($fullUrl, $shortenedUrl) {
        
        if ($this->getFullUrl($shortenedUrl) !== false) {
            // This means that we didn't check to see if the url was taken.
            // We shouldn't get here.
            throw new \Exception('Attempted to store shortened url that is already taken.');
        }
        // shortened url is not taken, go ahead and save this relationship
        $stmt = $this->DB_CONN->prepare('INSERT INTO ' . $this->DB_TABLE . ' (redirectUrl, shortenedPart) values (?, ?);');
        if (!$stmt) {
            throw new \Exception('Could not prepare statement.');
        }
        if (!$stmt->bind_param('ss', $fullUrl, $shortenedUrl)) {
            throw new \Exception('Could not bind parameters.');
        }
        if (!$stmt->execute()) {
            throw new \Exception('Could not execute statement.');
        }
        $stmt->close();
        return true;
        
    }

    // Used for redirect, as well as to check if shortenedUrl already taken
    public function getFullUrl($shortenedUrl) {
        $redirectUrl;
        $shortenedPart;
        
        $stmt = $this->DB_CONN->prepare('SELECT redirectUrl, shortenedPart FROM ' . $this->DB_TABLE . ' WHERE shortenedPart=?;');
        if (!$stmt) {
            throw new \Exception('Could not prepare statement.');
        }
        if (!$stmt->bind_param('s', $shortenedUrl)) {
            throw new \Exception('Could not bind parameters.');
        }
        if (!$stmt->execute()) {
            throw new \Exception('Could not execute statement.');
        }
        if (!$stmt->store_result()) {
            throw new \Exception('Could not store result.');
        }
        if ($stmt->num_rows === 0) {
            return false;
        }
        // Make sure we only get one result. This should probably never throw an error.
        if ($stmt->num_rows !== 1) {
            throw new \Exception('1 row expected, ' . $stmt->num_rows . ' row(s) found.');
        }
        
        $stmt->bind_result($redirectUrl, $shortenedPart);
        $stmt->fetch();
        
        $stmt->free_result();
        $stmt->close();
        
        return $redirectUrl;
    }
    
    /**
     * Returns an array of all shortened urls, with optional offset and numer of rows
     *
     * @param int $offset Offset for pagnation (optional)
     * @param int $numOfRows Number of records to retrieve (optional)
     * @return array
     */
    public function get($offset = 0, $numOfRows = 20) {
        $rows = [];
        
        $result = $this->DB_CONN->query('SELECT id, redirectUrl, shortenedPart FROM ' . $this->DB_TABLE);
        if (!$result) {
            throw new \Exception('Could not execute statement.');
        }
        
        while ($row = $result->fetch_row()) {
            array_push($rows,
                       ['id' => $row[0],
                        'redirectUrl' => $row[1],
                        'shortenedPart' => $row[2]]);
        }
        
        $result->close();
        
        return $rows;
    }
    
    /**
     * Returns the number of shortened URLs
     *
     * @return int
     */
    public function count() {
        $rowCount;
        $result = $this->DB_CONN->query('SELECT COUNT(*) FROM ' . $this->DB_TABLE);
        if (!$result) {
            throw new \Exception('Could not execute statement.');
        }
        
        $row = $result->fetch_row();
        $result->close();
        
        if (count($row) !== 1 || !is_array($row)) {
            throw new \Exception('Unexpected result from SELECT COUNT: ' . print_r($row, true));
        }
        
        return $row[0];
    }
}