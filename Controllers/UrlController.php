<?php
namespace Controllers;
use Models\UrlModel;
use Controllers\ResponseController;
use Controllers\RedirectController;

/*
 * Class used to handle requests to create a shortened URL.
 * Creates the shortened URL using the UrlModel
 */
class UrlController {
    private $UrlModel;
    private $response;

    public function __construct() {
        $this->UrlModel = new UrlModel();
        $this->response = new ResponseController();
    }
    
    // Shortened url will always be a string, so that we can keep it as a relative URL
    public function create($originalUrl, $shortenedUrl = null) {      
        //make sure we are getting passed a URL
        if (empty($originalUrl)) {
            $this->response->setError();
            $this->response->setMessage('Please supply a URL to be shortened.');
            return $this->response->json();
        }
        
        // Verify that the input is a valid URL
        if (!$this->isValidUrl($originalUrl)) {
            // Return message to indicate that
            $this->response->setError();
            $this->response->setInfo('originalUrl', $originalUrl);
            $this->response->setMessage('That is not a valid URL.');
            return $this->response->json();
        }
        
        // check if the shortenedUrl already exists
        if ( !empty($shortenedUrl) && $this->UrlModel->getFullUrl($shortenedUrl) !== false) {
            // Return something here to indicate that the shortened Url
            // being requested is already taken. Maybe suggest alternatives?
            $this->response->setError();
            $this->response->setMessage('That URL is already taken.');
            return $this->response->json();
        }
        
        // No url supplied, so generate our own.
        if (empty($shortenedUrl)) {
            $shortenedUrl = $this->generateRandomUrl();
        }
        
        $result = $this->UrlModel->create($originalUrl, $shortenedUrl);
        
        if($result) {
            $this->response->setMessage('Successfully created url.');
            $this->response->setInfo('originalUrl',$originalUrl);
            $this->response->setInfo('newUrl', $shortenedUrl);
        } else {
            $this->response->setError();
            $this->response->setInfo('originalUrl', $originalUrl);
            $this->response->setInfo('shortenedUrl', $shortenedUrl);
            $this->response->setMessage('There was an error shortening your URL.');
        }
        
        return $this->response->json();
        
    }
    
    private function isValidUrl($url) {
        return filter_var($url, FILTER_VALIDATE_URL);
    }
    
    private function generateRandomUrl() {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randString;
        while (!isset($randString) || $this->UrlModel->getFullUrl($randString) === true) {
            $randString = '';
            for ($i = 0; $i < 10; $i++) {
                $randString .= $characters[rand(0, strlen($characters)-1)];
            }
        }

        return $randString;
        
    }    

}