<?php
namespace Controllers;

class ResponseController {
    private $response;
    
    public function __construct() {
        $this->response = [
            'status' => null,
            'message' => null
        ];
        $this->setSuccess();
    }
    
    public function setInfo($text, $info) {
        $this->response[$text] = $info;
    }
    
    public function setMessage($m) {
        $this->setInfo('message', $m);
    }
    
    public function setError() {
        $this->setInfo('status','error');
    }
    
    public function setSuccess() {
        $this->setInfo('status','success');
    }
    
    public function json() {
        return json_encode($this->response);
    }
}