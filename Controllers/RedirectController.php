<?php
namespace Controllers;
use Models\UrlModel;
/**
 * This controller handles the redirect action when someone requests
 * a shortened url. Simply echo the output of redirect($url), and it
 * will take care of the rest.
 */
class RedirectController extends Controller {

    public function __construct() {
        
    }
    
    public function redirect($shortenedUrl) {
        $UrlModel = new UrlModel();
        $redirectUrl = $UrlModel->getFullUrl($shortenedUrl);
        if ($redirectUrl === false) {
            return header("Location: /#invalid");
        }
        return header("Location: " . $redirectUrl);
    }
}